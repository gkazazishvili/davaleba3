package com.example.newtest

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Color.red
import android.media.VolumeShaper
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import java.lang.*
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.newtest.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    var container = mutableListOf<String>()
    var activeCounter:Int = 0
    var deleteCounter:Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

    }

    private fun init(){
        binding.activeMem.text = activeCounter.toString()
        binding.deletedMem.text = activeCounter.toString()
        binding.savebtn.setOnClickListener {

                addUser()
                validateEmail()
                checkAge()

        }

        binding.clearbtn.setOnClickListener {
            removeUser()
            validateEmail()
            checkAge()
        }


        binding.updatebtn.setOnClickListener {
            updateUser()
            validateEmail()
            checkAge()

        }


    }


    private fun addUser(){
        if(binding.email.text.isNotEmpty() &&
            binding.firstName.text.isNotEmpty() &&
            binding.lastName.text.isNotEmpty() &&
            binding.age.text.isNotEmpty()){
            if(container.contains(binding.email.text.toString()) ||
                container.contains(binding.firstName.text.toString()) ||
                container.contains(binding.lastName.text.toString())){
                    clearAllFields()
                binding.errSucc.text = "Error"
                binding.errSucc.setTextColor(Color.RED)

                Toast.makeText(this, "User Already Exists", Toast.LENGTH_SHORT).show()
            } else{
                container.add(binding.email.text.toString())
                container.add(binding.firstName.text.toString())
                container.add(binding.lastName.text.toString())
                container.add(binding.age.text.toString())
                activeCounter++
                binding.activeMem.text = activeCounter.toString()
                binding.errSucc.text = "Success"
                binding.errSucc.setTextColor(Color.GREEN)

                clearAllFields()
                Toast.makeText(this, "User Added Successfully", Toast.LENGTH_SHORT).show()
            }

        } else{

            binding.errSucc.text = "Error"
            binding.errSucc.setTextColor(Color.RED)

            Toast.makeText(this, "ყველა ველი უნდა იყოს შევსებული", Toast.LENGTH_SHORT).show()
        }
    }


    private fun removeUser(){
        if(binding.email.text.isNotEmpty() ||
            binding.firstName.text.isNotEmpty() ||
            binding.lastName.text.isNotEmpty() ||
            binding.age.text.isNotEmpty()){
            if(container.contains(binding.email.text.toString()) &&
                container.contains(binding.firstName.text.toString()) &&
                container.contains(binding.lastName.text.toString())){

                container.remove(binding.email.text.toString())
                container.remove(binding.firstName.text.toString())
                container.remove(binding.lastName.text.toString())
                binding.errSucc.text = "Error"
                binding.errSucc.setTextColor(Color.RED)

                if(activeCounter.toString().toInt() != 0) {
                    activeCounter--
                    deleteCounter++
                    binding.activeMem.text = activeCounter.toString()
                    binding.deletedMem.text = activeCounter.toString()
                }


                clearAllFields()
                binding.errSucc.text = "Success"
                binding.errSucc.setTextColor(Color.GREEN)

                Toast.makeText(this, "User Removed Successfully", Toast.LENGTH_SHORT).show()

            } else{
                binding.errSucc.text = "Error"
                binding.errSucc.setTextColor(Color.RED)

                clearAllFields()
                Toast.makeText(this, "User doesn't exist", Toast.LENGTH_SHORT).show()

            }

        } else{
            binding.errSucc.text = "Error"
            binding.errSucc.setTextColor(Color.RED)

            Toast.makeText(this, "ყველა ველი უნდა იყოს შევსებული", Toast.LENGTH_SHORT).show()
        }
    }


    private fun updateUser(){
        if(binding.email.text.isNotEmpty() ||
            binding.firstName.text.isNotEmpty() ||
            binding.lastName.text.isNotEmpty() ||
            binding.age.text.isNotEmpty()){
            if(container.contains(binding.email.text.toString())){
                container.add(binding.email.text.toString())
                container.add(binding.firstName.text.toString())
                container.add(binding.lastName.text.toString())
                container.add(binding.age.text.toString())
                binding.errSucc.text = "Success"
                binding.errSucc.setTextColor(Color.GREEN)

                Toast.makeText(this, "User Updated", Toast.LENGTH_SHORT).show()
            } else{
                clearAllFields()
                binding.errSucc.text = "Error"
                binding.errSucc.setTextColor(Color.RED)

                Toast.makeText(this, "User doesn't exist", Toast.LENGTH_SHORT).show()
            }

        } else{
            binding.errSucc.text = "Error"
            binding.errSucc.setTextColor(Color.RED)
            Toast.makeText(this, "ყველა ველი უნდა იყოს შევსებული", Toast.LENGTH_SHORT).show()
        }
    }



    private fun clearAllFields(){
        binding.email.text.clear()
        binding.firstName.text.clear()
        binding.lastName.text.clear()
        binding.age.text.clear()

    }


    private fun validateEmail() {
        val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+"
        if (!binding.email.text.matches(emailPattern.toRegex())) {
            Toast.makeText(applicationContext, "Email არ არის ვალიდური",
                Toast.LENGTH_SHORT).show()
        }
    }

    private fun checkAge(){
        if(binding.age.text.isNotEmpty()) {
            if (binding.age.text.toString().toInt() < 1) {
                Toast.makeText(this, "ასაკი უნდა იყოს მთელი დადებითი რიცხვი", Toast.LENGTH_SHORT)
                    .show()
            }
        }

    }



}


